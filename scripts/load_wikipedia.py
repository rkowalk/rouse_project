import json
import time
import os
import google.cloud.storage
from sseclient import SSEClient as EventSource

#rather than spending time writing a recursive function to flatten a json object - grabbed another library
from flatten_json import flatten

url = 'https://stream.wikimedia.org/v2/stream/recentchange'
counter=0
storage_client = google.cloud.storage.Client.from_service_account_json('/home/ryankowalk/rouse_project_7e7df16d8c34')
bucket = storage_client.get_bucket('wikipedia-pipe')

#this list filters the input json down to specified fields, and fills default where no value exists
#currently it only contains a minimum set of values - more could be added in the future by updating this list
field_list = ["bot", "comment", "id", "length_new", "length_old", "meta_domain", "meta_dt", "meta_id"\
                 , "meta_request_id", "meta_schema_uri", "meta_topic", "meta_uri", "meta_partition", "meta_offset"\
                 , "minor", "namespace", "parsedcomment", "revision_new", "revision_old", "server_name"\
                 , "server_script_path", "server_url", "timestamp", "title", "type", "user", "wiki"]
default = ''

for event in EventSource(url):
    
    #ideally filtering would happen at this stage - but I defer it to when I have altered the file to a dictionary
    if event.event == 'message':
        try:
            #sseclient passes back an iterable with JSON files from a kafka messageing queue
            #first I removed nesting from the json file to make it usable in relational data
            change = flatten(json.loads(event.data))
            
            #next I am standardizing the fields in each json
            change = {k: change[k] if k in change else default for k in field_list}
            
            #the created dictionary from above is transfered back into json format
            json_flattened = json.dumps(change)
            
            #filtering the values that should be processed
            if change['bot'] == False and change['meta_domain']=='en.wikipedia.org':
                try:
                    #if the list exists - append value to the 
                    json_list.append(str(json_flattened))
                except:
                    #if the list doesnt exists - create the list
                    json_list=[str(json_flattened)]
                counter = counter+1
                print(counter)
                #I disobeyed direct orders, 1000 transactions happens every 5 minutes
                #for decreasing the number of uploads to bigquery, I've altered it to 10k
                if counter == 10000:
                    counter = 0
                
                    #reset the counter and save the new-line json delimited file to a bucket
                    #below is the format for uploading to the bucket on gcp
                    d = 'data/wiki_output_'+str(time.time())+'.json'
                    d = bucket.blob(d)
                    d.upload_from_string('\n'.join(json_list))
                    json_list=[]
        except ValueError as e:
            pass
