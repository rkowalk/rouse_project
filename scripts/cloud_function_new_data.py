from google.cloud import bigquery

def upload_table(data, context):
    client = bigquery.Client()
    table_ref = client.dataset('wikipedia_changes').table('recent_changes')
    file = data['name']
    
    
    previous_rows = client.get_table(table_ref).num_rows
    assert previous_rows > 0
    
    job_config = bigquery.LoadJobConfig()
    job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
    job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    uri = 'gs://wikipedia-pipe/'+file
    load_job = client.load_table_from_uri(
                                          uri,
                                          table_ref,
                                          job_config=job_config)  # API request
        print('Starting job {}'.format(load_job.job_id))
                                          
                                          load_job.result()  # Waits for table load to complete.
                                          print('Job finished.')
                                          
                                          destination_table = client.get_table(table_ref)
                                          print('Loaded {} rows.'.format(destination_table.num_rows))
