# Rouse Wikipedia Project

Documentation regarding the project for Rouse regarding recent edits to wikipedia pages

What were some of the design considerations
  - Stay within 'free' limitation on GCP
  - Balance performance while maintaining free limitation
  - Completed reasonably quickly during the alloted timeframe

Why was this technology stack chosen and implemented
  - The request for this project was not deeply analytical - but needed some ability in the future to enable analytical functions potentially. As well, completing the project on gcp was noted as a extra task.
  - As such, I built the ingestion using VM's because of the long running nature of the script. It would have become too expensive on Cloud Functions alone, didn't require a messaging queue(as there are no other users of the data) and did not have an analytical need for DataProc
  - Storing the data as JSON of the original allowed me to satisfy both the logging requirement as well as potential future analytical uses at a cheap rate( $0.02 per GB )
  - Big Query was requested as the data store. Streaming into bigquery is prohibitively expensive unless the project requires it. As well, bigquery doesnt perform well as an OLTP. So ideally bulk loads work best with BigQuery.

Pros and cons of this technology stack
  - Because of the nature of the pipeline, this will not scale beyond its current scope. That being said, it is fully capable of consuming the full output of wikipedias changes. Regarding the VM script - it potentially could be altered to attach to a pub/sub topic in order to scale past single system limitations. BigQuery is limited only in being a less than suitable OLTP system when doing bulk uploads - in order for it to perform well it requires streaming data in cache which becomes very expensive.

Alternatives to the implemented stack
   - The tech stack could have been built with a messaging queue to allow for more inputs than a single system would allow - doing a one-one on incoming messaging. I didnt see this necessary considering the current and future outlook for the project. Adding a queueing system will improve the ceiling performance, but will significantly increase costs.
   - The system could as well be built on BigTable for faster analytical queries, or even on Cloud SQL for a cheaper OLTP alternative. BigTable would be significantly more expensive, while Cloud SQL wouldn't have the ability to scale past 20TB.

# Example BigQuery
total number of records
'''sh
bq  query --use_legacy_sql=False  "SELECT COUNT(*) FROM 'rouse_project'.'wikipedia_changes'.'wikitest'"
'''
the top 10 contributing users
bq  query --use_legacy_sql=False  "SELECT user, COUNT(*) FROM 'rouse_project'.'wikipedia_changes'.'wikitest' GROUP BY user ORDER DESC LIMIT 10"


Requirements

```sh
$ pip install google-cloud-storage
$ pip install sseclient
$ pip install flatten_json
$ pip install python3
```


